# apptio-gsuite-addon

Opensource addon for Google Sheets to pull data in from Apptio

Install Instructions:
1. Open the Google Sheet you want to import data into.
2. Go to Tools > Script Editor
3. Copy Code.gs into Code.gs
4. Go File > New > HTML File and name it Input
5. Copy Input.html into Input.html
