/*
This script is to pull data from an Apptio report / table and put it into a Google Sheet
There are six inputs needed
 - Public Key
 - Private Key
 - Report URL
 - country
 - customer
 - environment
 
 
 From this data the script then works out what the OpenToken is and with that, then works out
 what the environment token is as both of these are required to pull the report.
 
 The final step is pulling the details from the report with the OpenToken and Environment token
 
 */

function onOpen() {
  SpreadsheetApp.getUi()
  .createMenu('Apptio')
  .addItem('Add / Change Apptio details', 'apptioInput')
  .addItem('Update data', 'importApptioData')
  .addToUi()
}

function apptioInput() {
  var html = HtmlService.createHtmlOutputFromFile('Input')
  .setWidth(400)
  .setHeight(400);
  SpreadsheetApp.getUi()
  .showModalDialog(html, 'Apptio Settings');
}

function processForm(formObject) {
  
  var publicKey = formObject.publicKey;
  var privateKey = formObject.privateKey;
  var url = formObject.url;
  var customer = formObject.customer;
  var enviro = formObject.enviro;
  var location = formObject.location;
  var sheet = formObject.sheet;
  var rows = formObject.rows;
  var fetchUrl = url + "&rowCount=" + rows
  
  if (location == 'eu') {
    var openTokenUrl = 'https://frontdoor-eu.apptio.com/service/apikeylogin';
    var environmentUrl = 'https://frontdoor-eu.apptio.com/api/environment/'+ customer + '/' + enviro;
  }
  else if (location == 'au') {
    var openTokenUrl = 'https://frontdoor-au.apptio.com/service/apikeylogin';
    var environmentUrl = 'https://frontdoor-au.apptio.com/api/environment/'+ customer + '/' + enviro;
  }
  else {
    var openTokenUrl = 'https://frontdoor.apptio.com/service/apikeylogin';
    var environmentUrl = 'https://frontdoor.apptio.com/api/environment/'+ customer + '/' + enviro;
  }
  
  var documentProperties = PropertiesService.getDocumentProperties();
  
  documentProperties.setProperties({
    'publicKey' : publicKey,
    'url' : url,
    'openTokenUrl' : openTokenUrl,
    'environmentUrl' : environmentUrl,
    'sheet' : sheet,
    'customer' : customer,
    'enviro' : enviro,
    'location' : location,
    'rows': rows,
    'fetchUrl' : fetchUrl
    
  });
  
  if (privateKey != "********") {
    documentProperties.setProperty('privateKey', privateKey)
  }
}

function getCurrentValues() {
  
  var docProperties = PropertiesService.getDocumentProperties();
  
  var data = docProperties.getProperties();
  
  //Logger.log(data);
  
  return data;

}

function getOpenToken(publicKey,privateKey,url) {
  var data = {
    "keyAccess": publicKey,
    "keySecret" : privateKey
  }
  
  var headers =  {
    'Accept' : 'application/json',
    'content-type' : 'application/json'
  }  
  
  var options = {
    'method' : 'post',
    'headers' : headers,
    'payload' : JSON.stringify(data),
    'muteHttpExceptions' : true,
  }
  
  var response = UrlFetchApp.fetch(url, options);
  
  var apptioOpenToken = response.getHeaders()['Set-Cookie','apptio-opentoken'];
  
  return apptioOpenToken
   
}

function getEnvironment(apptioOpenToken, url) {
  
  var headers = {
    'accept' : 'application/json',
    'apptio-opentoken' : apptioOpenToken
  }
  
  var options = {
    'method' : 'get',
    'headers' : headers
  }
  
  var response = UrlFetchApp.fetch(url,options);
  
  var environment = JSON.parse(response.getContentText())['id'];
  
  return environment
  
}



function importApptioData() {
  
  var docProperties = PropertiesService.getDocumentProperties();
  
  var publicKey = docProperties.getProperty('publicKey');
  var privateKey = docProperties.getProperty('privateKey');
  var url = docProperties.getProperty('fetchUrl');
  var openTokenUrl = docProperties.getProperty('openTokenUrl');
  var environmentUrl = docProperties.getProperty('environmentUrl');
  var sheet = docProperties.getProperty('sheet');
  
  var apptioOpenToken = getOpenToken(publicKey,privateKey,openTokenUrl);
  var environment = getEnvironment(apptioOpenToken,environmentUrl);
  
  var headers = 
      {
        "apptio-opentoken" : apptioOpenToken,
        "apptio-current-environment" : environment,
        "Accept" : "text/tab-separated-values",
      };
  
  var options =  
      {
        "method" : "get",
        "headers" : headers,
        "muteHttpExceptions" : true,
      }
  
  var contentAll = UrlFetchApp.fetch(url, options).getContentText(); 
  
  var dataAll = Utilities.parseCsv(contentAll, "\t");
  
  var sheet = SpreadsheetApp.getActive().getRangeByName(sheet);
  var row = sheet.getRow();
  var col = sheet.getColumn();
  var sheeta = sheet.getSheet();
  sheeta.getRange(row, col, dataAll.length, dataAll[0].length).setValues(dataAll);
  
}